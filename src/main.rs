use std::{env, fs, io};
use sha2::{Digest, Sha256};

struct Fingerprints {
    ecoji: String,
    hex: String,
}

fn print_fingerprints(filename: &String) -> Result<Fingerprints, io::Error> {
    let contents = fs::read(filename.clone())?;
    let result = Sha256::digest(&contents);
    let fingerprint: &[u8] = result.as_slice();

    Ok(Fingerprints {
        ecoji: ecoji::encode_to_string(&mut fingerprint.clone())?,
        hex: hex::encode(fingerprint)
    })
}

fn main() {
    for arg in env::args().skip(1) {
        let fingerprints = match print_fingerprints(&arg) {
            Ok(fingerprints) => fingerprints,
            Err(e) => {
                eprintln!("Couldn't generate fingerprints for {}: {}", arg, e);
                continue;
            },
        };

        println!("SHA-256 fingerprint for {}:", arg);
        println!("      hex: {}", fingerprints.hex);
        println!("    ecoji: {}", fingerprints.ecoji);
    }
}
